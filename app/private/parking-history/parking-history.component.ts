import {Component, OnInit} from '@angular/core';
import {HistoryService} from "~/services/history.service";
import {HistoryItem} from "~/services/models/history-item.model";
import {finalize, map} from "rxjs/operators";

@Component({
    moduleId: module.id,
    selector: 'app-parking-history',
    templateUrl: './parking-history.component.html',
    styleUrls: ['./parking-history.component.css'],
    providers: [HistoryService]
})
export class ParkingHistoryComponent implements OnInit {

    busy = false;
    items: HistoryItem[];

    constructor(private history: HistoryService) {
    }

    ngOnInit() {
        this.loadHistory();
    }

    private loadHistory() {
        this.busy = true;
        this.history.historyFor6Months()
            .pipe(
                finalize(() => this.busy = false)
            )
            .subscribe((items: HistoryItem[]) => this.items = items);
    }
}
