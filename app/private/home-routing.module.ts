import {NgModule} from "@angular/core";
import {Routes} from "@angular/router";
import {NativeScriptRouterModule} from "nativescript-angular/router";

import {HomeComponent} from "./home/home.component";
import {ParkingHistoryComponent} from "~/private/parking-history/parking-history.component";
import {FaqComponent} from "~/private/faq/faq.component";

const routes: Routes = [
    {path: "", component: HomeComponent},
    {path: "parking-history", component: ParkingHistoryComponent},
    {path: "faq", component: FaqComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class HomeRoutingModule {
}
