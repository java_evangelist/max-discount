import {Component, OnInit} from '@angular/core';
import {Page} from "tns-core-modules/ui/page";
import {DEFAULT_QUESTIONS} from "~/private/faq/questions";

@Component({
    moduleId: module.id,
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

    public questions = [];

    constructor(private _page: Page) {
    }

    ngOnInit() {
        this._page.actionBarHidden = true;
        this.questions = [{
            question: 'Как дела?',
            items: [{answer: 'Нормально.'}]
        }, {
            question: 'Какая комиссия от платежей за парковку?',
            items: [{answer: 'Нет комиссии и оплата идёт прямиком на твой личный счёт в «Московский паркинг».'}]
        }, {
            question: 'Штрафы не приходят?',
            items: [{answer: 'Штрафы не приходят. Проверял на 300-т разных машинах в течении 3-х месяцев. Если придёт – пиши, разберусь. Если прога накосячила – компенсирую за свой счёт.'}]
        }, {
            question: 'Это вообще легально?',
            items: [{answer: 'Да. Ты можешь и сам так играться, сидеть с секундомером и каждые 9 минут включать и выключать парковку. Всё в рамках правил.'}]
        }];
    }

}
