import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {NativeScriptCommonModule} from "nativescript-angular/common";

import {HomeRoutingModule} from "./home-routing.module";
import {HomeComponent} from "./home/home.component";
import {NativeScriptUISideDrawerModule} from "nativescript-ui-sidedrawer/angular";
import {ParkingHistoryComponent} from "~/private/parking-history/parking-history.component";
import {ModalDialogService, NativeScriptFormsModule} from "nativescript-angular";
import {SelectCarComponent} from "~/private/car/select-car/select-car.component";
import {CreateCarComponent} from "~/private/car/create-car/create-car.component";
import {CarComponent} from "~/private/car/car.component";
import {FaqComponent} from "~/private/faq/faq.component";
import {AccordionModule} from "nativescript-accordion/angular";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        HomeRoutingModule,
        NativeScriptUISideDrawerModule,
        NativeScriptFormsModule,
        AccordionModule
    ],
    declarations: [
        HomeComponent,
        ParkingHistoryComponent,
        SelectCarComponent,
        CreateCarComponent,
        CarComponent,
        FaqComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    entryComponents: [
        SelectCarComponent,
        CreateCarComponent
    ],
    providers: [
        ModalDialogService
    ]
})
export class HomeModule {
}
