import {Component, OnInit} from '@angular/core';
import {ModalDialogParams} from "nativescript-angular";
import * as dialogs from "tns-core-modules/ui/dialogs";
import {finalize} from "rxjs/operators";
import {CarsService} from "~/services/cars.service";
import CarRequestModel from "~/services/models/CarRequestModel";

@Component({
    moduleId: module.id,
    selector: 'CreateCar',
    templateUrl: './create-car.component.html',
    styleUrls: ['./create-car.component.css'],
    providers: [CarsService]
})
export class CreateCarComponent implements OnInit {

    public model: CarRequestModel = new CarRequestModel();
    public busy: boolean = false;

    constructor(private _params: ModalDialogParams, private _cars: CarsService) {
    }

    ngOnInit() {
    }

    create() {
        if (!this.model.carNo) {
            return this.validationAlert();
        }

        this.busy = true;
        this._cars.create(this.model)
            .pipe(
                finalize(() => this.busy = false)
            )
            .subscribe(() => this.close())
    }

    private validationAlert() {
        return dialogs.alert({
            title: 'Данные некорректны',
            message: 'Укажите номер автомобиля',
            okButtonText: 'Ок'
        });
    }

    close() {
        this._params.closeCallback(this.model.carNo);
    }

}
