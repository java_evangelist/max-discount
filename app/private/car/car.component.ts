import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewContainerRef} from '@angular/core';
import {SelectCarComponent} from "~/private/car/select-car/select-car.component";
import {CreateCarComponent} from "~/private/car/create-car/create-car.component";
import {ModalDialogService} from "nativescript-angular";
import {StorageService} from "~/services/storage.service";

const _SELECTED_CAR_KEY = 'selectedCarNo';

/*
 * TODO: О костыле
 *
 * Здесь реализован костыль, у нас есть входной
 * параметр carNo и есть кеш в котором сохранено
 * прошлое значение которое выбирал пользователь,
 * и когда мы инициализируем этот компонент, мы
 * быстро достаем значение из кеша и потом оно
 * перезатирается значением из входного параметра.
 *
 * Но в кеше значения может и не быть, по-этому у
 * нас есть костыль в виде метода ngOnChanges.
 *
 * Это можно пофиксить позже, погуглить, разобраться,
 * но щас все делается в попыхах и на это нет времени
 */
@Component({
    moduleId: module.id,
    selector: 'Car',
    templateUrl: './car.component.html',
    styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit, OnChanges {

    /* Это хак, для того чтобы сохранять выбранный номер автомобиля,
     * при каждом изменении свойства, переделать это на нормальное
     * решение после релиза */
    @Input() public carNo: string;

    public selectedCarNo: string;

    constructor(private _modals: ModalDialogService, private _view: ViewContainerRef, private _storage: StorageService) {
    }

    ngOnInit(): void {
        /* initialize component */
        this.selectedCarNo = this._storage.get(_SELECTED_CAR_KEY)
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['carNo'] && changes['carNo'].currentValue && !this.selectedCarNo) {
            this.selectedCarNo = changes['carNo'].currentValue;
        }
    }

    /**
     * Предлагает пользователю выбрать машину
     */
    selectCar() {
        if (!this.selectedCarNo) {
            // Если машин нет у юзера, то открываем
            // окно добавления новой
            return this.addNewCar();
        }

        this._modals.showModal(SelectCarComponent, {
            viewContainerRef: this._view,
            fullscreen: true,
            context: {currentCarNo: this.selectedCarNo}
        }).then((carNo: string) => {
            if (!carNo) {
                return;
            }
            this._putCarNo(carNo);
        });
    }

    public getCarNo(): string {
        return this.selectedCarNo;
    }

    private _putCarNo(val: string) {
        this.selectedCarNo = val;
        this._storage.put(_SELECTED_CAR_KEY, val);
    }

    /**
     * Этот метод должен открыть окно для добавления
     * новой машины в систему.
     */
    addNewCar() {
        this._modals.showModal(CreateCarComponent, {
            viewContainerRef: this._view,
            fullscreen: true
        }).then((newCarNo: string) => {
            if (!this.selectedCarNo) {
                this._putCarNo(newCarNo);
            }
        });
    }
}
