import {Component, OnInit} from '@angular/core';
import {ModalDialogParams} from "nativescript-angular";
import {CarDetails} from "~/services/models/home-overview.model";
import {CarsService} from "~/services/cars.service";
import {finalize} from "rxjs/operators";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    moduleId: module.id,
    selector: 'SelectCar',
    templateUrl: './select-car.component.html',
    styleUrls: ['./select-car.component.css']
})
export class SelectCarComponent implements OnInit {

    public availableCars: CarDetails[];
    public currentCarNo: string;

    constructor(private _params: ModalDialogParams, private _cars: CarsService) {
    }

    ngOnInit() {
        this.currentCarNo = this._params.context.currentCarNo;

        this.loadAvailableCarsFromBackend();
    }

    private loadAvailableCarsFromBackend() {
        this._cars.availableCarsForCurrentUser()
            .subscribe((cars: CarDetails[]) => this.availableCars = cars)
    }

    returnSelected(carId) {
        this._params.closeCallback(carId);
    }

}
