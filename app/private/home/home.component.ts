import {AfterViewInit, Component, OnInit, ViewChild} from "@angular/core";
import {AccountOverview} from "~/services/models/home-overview.model";
import {Page} from "tns-core-modules/ui/page";
import {AccountService} from "~/services/account.service";
import * as dialogs from "tns-core-modules/ui/dialogs";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import {RadSideDrawerComponent} from "nativescript-ui-sidedrawer/angular";
import {AuthService} from "~/services/auth.service";
import {ModalDialogService, RouterExtensions} from "nativescript-angular";
import {CarComponent} from "~/private/car/car.component";
import {finalize} from "rxjs/operators";
import {ParkingClientService} from "~/services/clients/parking-client.service";

// todo: отрефакторить
// todo: компонент сильно перегружен, разделить его на несколько
@Component({
    selector: "Home",
    moduleId: module.id,
    providers: [ModalDialogService],
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit, AfterViewInit {

    public parkingNo = new ParkingNumber();
    public overview: AccountOverview;
    public busy: boolean = false;
    public delayMinutes = 13;

    public isParked = false;

    @ViewChild(CarComponent) carComponent: CarComponent;
    @ViewChild(RadSideDrawerComponent) drawerComponent: RadSideDrawerComponent;
    drawer: RadSideDrawer;

    private parkingCode;

    constructor(
        private page: Page,
        private accounts: AccountService,
        private parking: ParkingClientService,
        private auth: AuthService,
        private router: RouterExtensions,
    ) {
        this.overview = new AccountOverview(undefined, 0, []);
    }

    ngOnInit(): void {
        this.page.actionBarHidden = true;

        this.parking.isParked().subscribe(this.parkingSessionHandler.bind(this), () => {/* do nothing */});
        this.loadAccountOverview().add(() => this.warningIfNoAvailableCars());
    }

    ngAfterViewInit(): void {
        this.drawer = this.drawerComponent.sideDrawer;
    }

    /**
     * Этот метод должен открыть навигацию бутерброд
     */
    showMenu() {
        this.drawer.showDrawer();
    }

    /**
     * Этот метод загружает основную информацию по аккаунту после
     * инициализации компонента.
     */
    public loadAccountOverview() {
        this.busy = true;
        return this.accounts.fullDetails()
            .pipe(
                finalize(() => this.busy = false)
            )
            .subscribe((data: AccountOverview) => {
                this.overview = data;
            });
    }

    private warningIfNoAvailableCars() {
        if (!this.overview.hasAvailableCars) {
            this.warning('У Вас нет автомобилей в аккаунте, мы не сможем Вас припарковать');
        }
    }

    private warning(message) {
        return dialogs.alert({
            message,
            title: 'Предупреждение',
            okButtonText: 'Ок'
        });
    }

    /**
     * Этот метод должен начинать парковку с переодическими отменами
     * этой парковки и очередными бронированиями
     */
    public parkingButtonTapped(): void {
        if (this.isParked) {
            this.stopParking();
        } else {
            this.startParking();
        }
    }

    /**
     * Останавливает парковку
     */
    private stopParking() {
        this.parking.stopParking(this.parkingCode)
            .subscribe(() => this.isParked = false)
            .add(() => this.loadAccountOverview());
    }

    /**
     * Паркует машину по специальному алгоритму для
     * высокой экономии
     */
    private startParking() {
        const errors = this.parkingNo.validationErrors();
        if (errors && errors.length) {
            return dialogs.alert({
                title: 'Данные некорректны',
                message: errors.join('\m'),
                okButtonText: 'Ок'
            })
        }
        // raise parking event
        this.parking.parkingSession(this.parkingNo.value, this.carComponent.getCarNo())
            .subscribe(this.parkingSessionHandler.bind(this));
    }

    private parkingSessionHandler(response: any): void {
        this.isParked = true;
        this.parkingCode = response.parkingKey;
    }

    /**
     * Делает выход из текущего аккаунта
     */
    logout() {
        this.auth.logout();

        this.router.navigate(['/'], {
            clearHistory: true,
            transition: {name: 'fade'}
        })
    }

    /**
     * Увеличивает задержку перед началом парковки
     */
    public increaseParkingStartDelay() {
        if (this.delayMinutes + 1 > 30) {
            return;
        }
        this.delayMinutes++;
    }

    /**
     * Уменьшает задержку перед началом парковки
     */
    public decreaseParkingStartDelay() {
        if (this.delayMinutes - 1 < 1) {
            return;
        }
        this.delayMinutes--;
    }

    get formattedDelayMinutes() {
        return this.delayMinutes < 10 ? '0' + this.delayMinutes : this.delayMinutes;
    }
}

const PARKING_NUM_REGEX = /[0-9]{4}/;

/**
 * Объект значение, он инкапсулирует в себе номер
 * парковки, этот объект так же позволяет сделать
 * валидацию значения номера
 */
class ParkingNumber {
    value: string;

    public validationErrors() {
        const errors = [];

        if (!this.value) {
            errors.push('Номер паровки не указан');
        } else {
            if (!PARKING_NUM_REGEX.test(this.value)) {
                errors.push('Номер парковки должен состоять только из чисел и должен быть длиной 4 цифры');
            }
        }

        return errors;
    }
}
