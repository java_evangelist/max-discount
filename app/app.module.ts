import {ErrorHandler, NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {AuthService} from "~/services/auth.service";
import {CommonRequestService} from "~/services/common-request.service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {CredentialsHolderService} from "~/services/credentials-holder.service";
import {AccountService} from "~/services/account.service";
import {ParkingClientService} from "~/services/clients/parking-client.service";
import {Fontawesome} from "nativescript-fontawesome";
import {CarsService} from "~/services/cars.service";
import {RequestInterceptor} from "~/RequestInterceptor";
import {AppErrorHandler} from "~/AppErrorHandler";
import {StorageService} from "~/services/storage.service";

Fontawesome.init();

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        HttpClientModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        AccountService,
        AuthService,
        CommonRequestService,
        CredentialsHolderService,
        ParkingClientService,
        CarsService,
        StorageService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: RequestInterceptor,
            multi: true
        },
        {
            provide: ErrorHandler,
            useClass: AppErrorHandler
        }
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule {
}
