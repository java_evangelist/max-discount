import {Injectable} from '@angular/core';
import {CommonRequestService} from "~/services/common-request.service";
import {map, retry} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {CredentialsHolderService} from "~/services/credentials-holder.service";

@Injectable()
export class ParkingClientService {

    // todo: в конфигурацию
    private readonly baseUrl = 'http://3.121.16.239:8080';

    constructor(private http: HttpClient, private accHolder: CredentialsHolderService) {
    }

    /**
     * Этот метод начинает парковку от текущего
     * авторизированного пользователя.
     */
    parkingSession(zoneId: string, carNo: string) {
        const {phone, pin} = this.accHolder.credentials;

        return this.http.post(`${this.baseUrl}/parking`, {phone, pin, zoneId, carNo});
    }

    /**
     * Этот метод должен остановить парковку по определенной сессии
     */
    stopParking(code) {
        return this.http.delete(`${this.baseUrl}/parking/${code}`);
    }

    /**
     * Этот метод возвращает булево значение как статус парковки. Этот
     * метод вернет true если мы припаркованы и false в противном случае
     */
    isParked() {
        const {phone, pin} = this.accHolder.credentials;

        return this.http.get(`${this.baseUrl}/parking?phone=${encodeURIComponent(phone)}&pin=${pin}`);
    }
}
