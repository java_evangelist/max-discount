import {Injectable} from '@angular/core';
import {CommonRequestService} from "~/services/common-request.service";
import CarRequestModel from "~/services/models/CarRequestModel";
import {AccountService} from "~/services/account.service";
import {map} from "rxjs/operators";
import {AccountOverview} from "~/services/models/home-overview.model";

@Injectable()
export class CarsService {

    constructor(private _request: CommonRequestService, private _account: AccountService) {
    }

    /**
     * Возвращает машины которые есть у пользователя в аккаунте.
     * Этот метод может загрузить пустой список. Никогда не
     * возвращает null или undefined
     */
    availableCarsForCurrentUser() {
        return this._account.details('carDetails')
            .pipe(
                map((overview: AccountOverview) => overview.availableCars)
            )
    }

    /**
     * Создает новый автомобиль в аккаунте пользователя
     *
     * @param request
     */
    create(request: CarRequestModel) {
        const isDefaultCar = +request.isDefault;

        return this._request.executeWithAuth({
            operation: 'car_add',
            data: `classID:DEFAULT\ncarNo:${request.carNo}\nisDefault:${isDefaultCar}\ndescription:${request.description}`
        })
    }
}
