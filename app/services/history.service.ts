import {Injectable} from '@angular/core';
import {CommonRequestService} from "~/services/common-request.service";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {HistoryItem} from "~/services/models/history-item.model";

/**
 * Сервис для работы с историей по аккаунту
 */
@Injectable()
export class HistoryService {

    private readonly SIX_MONTHS = 183;

    constructor(private http: CommonRequestService) {
    }

    /**
     * Этот метод запрашивает историю за 6 месяцев по аккаунту.
     */
    historyFor6Months(): Observable<HistoryItem[]> {

        /*
         * Здесь запрашивается история ПРИМЕРНО ЗА 6 МЕСЯЦЕВ,
         * потому что в JS нет нормального метода для вычитания
         * дат, и приходится в ручную отсчитывать 120 дней, ну
         * и соответственно не учитываются месяца где 30 - 31 день
         * и февраль не исключение
         */

        const start = new Date();
        start.setDate(start.getDate() - this.SIX_MONTHS);

        return this.historyForRange(start, new Date());
    }

    /**
     * Запрашивает историю по аккаунту в периоде по датам
     *
     * @param start начало ленты истории
     * @param end конец ленты истории
     */
    historyForRange(start: Date, end: Date): Observable<HistoryItem[]> {
        const startTime = start.getTime() / 1000;
        const endTime = end.getTime() / 1000;

        return this.http.executeWithAuth({
            operation: 'account_history',
            data: `type:payment,parking\nstartTime:${startTime}\nstopTime:${endTime}`
        }).pipe(
            map((r: any) => {
                return (r.history || []).map(i => {
                    const row = i.historyRow;

                    return new HistoryItem(
                        row.type,
                        row.startTime,
                        row.duration,
                        row.info,
                        row.locationName,
                        row.carNo,
                        row.parkingFee
                    );
                }).sort((a, b) => b.startTime - a.startTime);
            })
        );
    }

}
