import {Injectable} from '@angular/core';
import {Login} from "~/security/login.model";
import {StorageService} from "~/services/storage.service";

@Injectable()
export class CredentialsHolderService {

    private readonly accountKey = "account";

    constructor(private _storage: StorageService) {
    }

    putCredentials(data: Login) {
        this._storage.put(this.accountKey, data);
    }

    get credentials(): Login {
        return this._storage.get(this.accountKey);
    }

    get presentCredentials() {
        return this._storage.has(this.accountKey);
    }
}
