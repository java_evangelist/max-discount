import {Injectable} from '@angular/core';
import {setString, getString, remove, hasKey} from "tns-core-modules/application-settings";

const _ROOT_KEY_NAME = "_mx_d_app";

@Injectable()
export class StorageService {

    constructor() {
    }

    public put(k: string, v: any): void {
        const _db = this.readDb();

        _db[k] = v;

        setString(_ROOT_KEY_NAME, JSON.stringify(_db));
    }

    public get<T>(k: string, defaultValue?: any): T {
        const val = this.readDb()[k];

        return val || defaultValue;
    }

    public has(k: string): boolean {
        return !!this.readDb()[k];
    }

    public readDb() {
        return JSON.parse(getString(_ROOT_KEY_NAME) || '{}');
    }

    public clear() {
        remove(_ROOT_KEY_NAME);
    }

}
