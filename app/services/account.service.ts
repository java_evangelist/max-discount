import {Injectable} from '@angular/core';
import {CommonRequestService} from "~/services/common-request.service";
import {map} from "rxjs/operators";
import {AccountOverview} from "~/services/models/home-overview.model";

@Injectable()
export class AccountService {

    constructor(private executor: CommonRequestService) {
    }

    details(...details: string[]) {
        if (!details.length) throw new Error('can\'t load details, because details kinds not given');

        return this.executor.executeWithAuth({
            operation: 'account_check',
            data: details.join(',')
        }).pipe(
            map((r: any) => {
                const allAvailableCars = r.carsList || [];
                const defaultCarNo = allAvailableCars.filter(car => car.class === "DEFAULT").map(car => car.car)[0];

                return new AccountOverview(defaultCarNo, r.availableFunds, allAvailableCars);
            })
        )
    }

    /**
     * Загружает все необходимые данные по клиенту
     * для отображения на UI
     */
    fullDetails() {
        return this.details('carDetails', 'wallet');
    }
}
