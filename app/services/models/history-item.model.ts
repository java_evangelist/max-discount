export class HistoryItem {
    constructor(
        public type: string,
        public startTime: number,
        public duration: number,
        public info: string,
        public locationName: string,
        public carNo: string,
        public parkingFee: number) {
    }

    get formattedDuration() {
        // todo: во второй версии приложения, сделать адекватно

        const hours = this.formatZeroLeadingNum(Math.floor(this.duration / 3600));
        const minutes = this.formatZeroLeadingNum(Math.floor((this.duration % 3600) / 60));
        const seconds = this.formatZeroLeadingNum(Math.floor(this.duration % 60));

        return `${hours}:${minutes}:${seconds}`;
    }

    private formatZeroLeadingNum(value) {
        return value < 10 ? '0' + value : value;
    }
}