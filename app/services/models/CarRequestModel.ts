export default class CarRequestModel {
    public carNo: string;
    public description: string = "";
    public isDefault: boolean = false;
}
