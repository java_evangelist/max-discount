export class CarDetails {
    description: string;
    class: string;
    isDefault: boolean;
    car: string;
}

export class AccountOverview {
    readonly carNo: string;
    readonly availableCars: CarDetails[];

    readonly currentBalance: number;

    constructor(carNo?: string, currentBalance?: number, availableCars?: CarDetails[]) {
        this.carNo = carNo;
        this.currentBalance = Math.floor(currentBalance);
        this.availableCars = availableCars;
    }

    get hasAvailableCars() {
        return !!this.availableCars.length;
    }
}