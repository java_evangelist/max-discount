import {Injectable} from '@angular/core';
import {Login} from "~/security/login.model";
import {AccountService} from "~/services/account.service";
import {AccountOverview} from "~/services/models/home-overview.model";
import {CredentialsHolderService} from "~/services/credentials-holder.service";
import {tap} from "rxjs/operators";
import {CommonRequestService} from "~/services/common-request.service";
import {StorageService} from "~/services/storage.service";

@Injectable()
export class AuthService {

    constructor(
        private accounts: AccountService,
        private accHolder: CredentialsHolderService,
        private commonRequest: CommonRequestService,
        private storage: StorageService
    ) {
    }

    authenticate(login: Login) {
        return this.commonRequest.execute(login, {
            operation: 'account_check'
        }).pipe(
            tap(() => this.accHolder.putCredentials(login))
        );
    }

    logout() {
        // Чистим вообще ВСЕ
        this.storage.clear();
    }
}
