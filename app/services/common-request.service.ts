import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Login} from "~/security/login.model";
import {map} from "rxjs/operators";
import {CredentialsHolderService} from "~/services/credentials-holder.service";

/**
 * Общий сервис для исполнения запросов, тут абстрагирована
 * основная логика исполнения запроса
 */
@Injectable()
export class CommonRequestService {

    private readonly apiUrl = 'https://parkingcab.mos.ru/AppHTTP.php';

    private readonly defaultHeaders = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
    });

    constructor(private http: HttpClient, private accHolder: CredentialsHolderService) {
    }

    executeWithAuth(body: object) {
        if (!this.accHolder.presentCredentials) {
            throw new Error('Credentials not present in acc holder!');
        }
        return this.execute(this.accHolder.credentials, body);
    }

    /**
     * Выполняет запрос с логином и паролем из
     * storage. Этот метод выбросит ошибку, если
     * логина и пароля в сторе нет. Если известен
     * заранее логин и пароль, то можно использовать
     * другую версию метода, которая принимает
     * логин с паролем
     */
    execute(login: Login, body: object) {
        const req = this.formData(login, body);

        return this.http.post(this.apiUrl, req, {headers: this.defaultHeaders})
            .pipe(
                /*
                 * Этот map необходим потому что бекенд отсылает
                 * ошибки клиента как статус 200, вместо 400+ кодов,
                 * по-этому Angular Http client распознает их как
                 * успешный ответ, чтобы обеспечить нормальную
                 * обработку ошибок, мы делаем эту штуку, плюс чтобы
                 * вернуть еще завернутый результат в response
                 */
                map(CommonRequestService.mappedResponse)
            )
    }

    private static mappedResponse(r: any) {
        if (r.response.errorCode) {
            throw new Error(r.response.errorInfo);
        }
        return r.response;
    }

    /**
     * Этот метод должен собрать нам formData которую
     * мы отправим на сервер, да, сервер принимает
     * данные не как обычный payload, а как form data
     *
     * @param login данные авторизации, токенов в этом
     * api нет, по-этому надо постоянно посылать логин
     * с пин кодом
     * @param body дополнительные данные
     */
    private formData(login: Login, body: object) {
        const formData = new FormData();

        formData.append("phoneNo", login.phone);
        formData.append("PIN", login.pin);
        formData.append("format", "json");

        Object.keys(body).forEach(k => formData.append(k, body[k]));

        return formData;
    }

}
