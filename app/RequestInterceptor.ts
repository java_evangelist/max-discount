import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
} from '@angular/common/http';

import {Observable, throwError} from "rxjs";
import {retry, retryWhen, timeoutWith} from "rxjs/operators";

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            retry(4),
            timeoutWith(4500, throwError(new Error('Сервер временно не доступен'))),
        );
    }
}

