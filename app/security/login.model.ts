/**
 * Основной класс этого модуля, он используется
 * для того чтобы передаваться в авторизации в
 * системе паркинга
 */
export class Login {
    public phone: string;
    public pin: string;

    validator() {
        return new Validator(this);
    }
}

/**
 * Этот класс используется для того чтобы валидировать форму
 * авторизации. Это мутабельный класс, и получать его надо
 * только через класс <tt>Login</tt>
 */
class Validator {
    private errors: string[] = [];

    constructor(private login: Login) {
    }

    validate() {
        this.validatePin();
        this.validatePhone();
    }

    private validatePin() {
        if (!this.login.pin) {
            this.errors.push('Введите пин код');
        } else if (this.login.pin && this.login.pin.length != 4) {
            this.errors.push('Пин код должен состоять из 4 цифр');
        }
    }

    private validatePhone() {
        if (!this.login.phone) {
            this.errors.push('Введите номер телефона');
        }
    }

    get hasErrors() {
        return this.errors.length > 0;
    }

    get errorsString() {
        return this.errors.join("\n");
    }
}