import {Component, OnInit} from '@angular/core';
import {Page} from "tns-core-modules/ui/page";
import {Login} from "~/security/login.model";
import {AuthService} from "~/services/auth.service";
import {RouterExtensions} from "nativescript-angular";
import * as dialogs from "ui/dialogs";
import {finalize} from "rxjs/operators";

@Component({
    moduleId: module.id,
    selector: 'app-security',
    templateUrl: './security.component.html',
    styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {

    login: Login = new Login();
    busy: boolean = false;

    constructor(
        private page: Page,
        private router: RouterExtensions,
        private auth: AuthService,
    ) {
    }

    ngOnInit() {
        this.page.actionBarHidden = true;
        this.page.backgroundColor = "#FFC905";
    }

    helpAboutPin() {
        dialogs.alert({
            title: 'Пин код',
            message: 'Ваш пин код для приложения «Парковки Москвы»\n' +
                'Этот пин код вы можете узнать отправив по СМС слово «pin» на номер Московского паркинга 7757',
            okButtonText: 'Ок, понятно'
        })
    }

    /**
     * Так как в апи к с которым мы интегрируемся дурацкая
     * авторизация, там нет токенов, мы должны постоянно
     * посылать телефон и пин-код, мы тут пытаемся получить
     * их и сделать какой-нибудь запрос, чтобы узнать,
     * корректные они или нет
     */
    signIn() {
        const validator = this.login.validator();
        validator.validate();

        if (validator.hasErrors) {
            return dialogs.alert({
                title: 'Ошибка',
                message: validator.errorsString,
                okButtonText: 'Ок'
            });
        }

        this.authenticate();
    }

    private authenticate() {
        this.busy = true;
        this.auth.authenticate(this.login)
            .pipe(
                finalize(() => this.busy = false)
            )
            .subscribe(() => this.redirectToHome());
    }

    private redirectToHome() {
        this.router.navigate(['/home'], {
            clearHistory: true,
            transition: {name: 'fade'}
        });
    }
}
