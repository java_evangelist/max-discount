import {Routes} from '@angular/router';
import {SecurityComponent} from "~/security/security/security.component";
// app

export const SecurityRoutes: Routes = [
    {
        path: '',
        component: SecurityComponent
    }
];
