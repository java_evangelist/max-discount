import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import {NativeScriptFormsModule} from 'nativescript-angular/forms';
import {NativeScriptRouterModule} from 'nativescript-angular';

import {SecurityRoutes} from './security.routes';
import {SecurityComponent} from './security/security.component';
import {MaskedTextFieldModule} from "nativescript-masked-text-field/angular";

@NgModule({
    imports: [
        NativeScriptFormsModule,
        NativeScriptRouterModule.forChild(<any>SecurityRoutes),
        MaskedTextFieldModule
    ],
    declarations: [
        SecurityComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SecurityModule {
}
