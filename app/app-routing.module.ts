import {NgModule} from "@angular/core";
import {Routes} from "@angular/router";
import {NativeScriptRouterModule} from "nativescript-angular/router";

// Common route addresses
export const HOME_ROUTE = "home";
export const SECURITY_ROUTE = "security";

const routes: Routes = [
    {path: "", redirectTo: "/security", pathMatch: "full"},
    {path: HOME_ROUTE, loadChildren: "~/private/home.module#HomeModule"},
    {path: SECURITY_ROUTE, loadChildren: "~/security/security.module#SecurityModule"}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {
}
