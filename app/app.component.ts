import {Component, OnInit} from "@angular/core";
import {RouterExtensions} from "nativescript-angular";
import {CredentialsHolderService} from "~/services/credentials-holder.service";
import {HOME_ROUTE} from "~/app-routing.module";

@Component({
    selector: "ns-app",
    template: '<page-router-outlet></page-router-outlet>'
})
export class AppComponent implements OnInit {

    constructor(
        private accHolder: CredentialsHolderService,
        private router: RouterExtensions
    ) {
    }

    ngOnInit(): void {
        if (this.accHolder.presentCredentials) {
            this.router.navigate([HOME_ROUTE], {
                clearHistory: true,
                animated: false
            });
        }
    }
}
