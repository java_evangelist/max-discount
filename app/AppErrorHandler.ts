import {ErrorHandler, Injectable} from '@angular/core';

import * as dialogs from "tns-core-modules/ui/dialogs";
import {HttpErrorResponse} from "@angular/common/http";

/**
 * Глобальный обработчик ошибок, который позволяет нам
 * ловить и обрабатывать ошибки системы одинаково и в
 * централизованном месте
 */
@Injectable()
export class AppErrorHandler implements ErrorHandler {
    handleError(error: any): void {
        if (error instanceof Error) {
            this.alert(error.message);

            return ;
        }

        this.handleAnotherExceptions(error);
    }

    private handleAnotherExceptions(error: any): void {
        if (error instanceof HttpErrorResponse) {
            console.log(error);
            if (error.status === 0) {
                dialogs.alert({
                    title: 'Ошибка',
                    message: 'Сервер временно не доступен',
                    okButtonText: 'Ок'
                })
            } else {
                this.alert(error.message);
            }
        }
    }

    private alert(message: string): void {
        dialogs.alert({
            title: 'Ошибка',
            message: message,
            okButtonText: 'Ок'
        });
    }
}